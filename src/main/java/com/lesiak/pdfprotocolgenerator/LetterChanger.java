package com.lesiak.pdfprotocolgenerator;

    public class LetterChanger {

        // This class change letters from Polish to 'Worldwide'.
        // TODO: Find a new solution.
        public static final String changeLetters(String stringInput){
            StringBuffer stringBuffer = new StringBuffer();

            for (int i = 0; i < stringInput.length(); i++) {
                char c = stringInput.charAt(i);
                switch (c) {
                    case 'ą': stringBuffer.append("a"); break;
                    case 'ć': stringBuffer.append("c"); break;
                    case 'ę': stringBuffer.append("e"); break;
                    case 'ł': stringBuffer.append("l"); break;
                    case 'ń': stringBuffer.append("n"); break;
                    case 'ó': stringBuffer.append("o"); break;
                    case 'ś': stringBuffer.append("s"); break;
                    case 'ź': stringBuffer.append("z"); break;
                    case 'ż': stringBuffer.append("z"); break;
                    case 'Ą': stringBuffer.append("A"); break;
                    case 'Ć': stringBuffer.append("C"); break;
                    case 'Ę': stringBuffer.append("E"); break;
                    case 'Ł': stringBuffer.append("L"); break;
                    case 'Ń': stringBuffer.append("N"); break;
                    case 'Ó': stringBuffer.append("O"); break;
                    case 'Ś': stringBuffer.append("S"); break;
                    case 'Ź': stringBuffer.append("Z"); break;
                    case 'Ż': stringBuffer.append("Z"); break;

                    default: stringBuffer.append(c); break;
                }
            }

            return stringBuffer.toString();
        }
}
