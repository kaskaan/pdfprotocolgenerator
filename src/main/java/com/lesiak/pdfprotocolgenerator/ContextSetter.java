package com.lesiak.pdfprotocolgenerator;

import org.thymeleaf.context.Context;

import java.util.HashMap;
import java.util.Map;

// This class map set up variables and values, and return Thymeleaf context. We can use this variables in HTML document;
// e.g. [[${clientFirstName}]] return us "Kowalski" value.
public class ContextSetter {
    private static Context context = new Context();
    private static Map<String, String> contextVariablesMap = new HashMap<>();

    static {
        contextVariablesMap.put("clientFirstName", "Mieczysław");
        contextVariablesMap.put("clientLastName", "Kowalski");
        contextVariablesMap.put("workerFirstName", "Marcin");
        contextVariablesMap.put("workerLastName", "Dudzic");
    }

    public static Context createNewContext() {
        contextVariablesMap.forEach((k, v) -> context.setVariable(k, v));
        return context;
    }

}
