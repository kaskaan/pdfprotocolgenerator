package com.lesiak.pdfprotocolgenerator;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerFontProvider;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

import java.awt.*;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import static org.thymeleaf.templatemode.TemplateMode.HTML;

public class PdfProtocolGenerator {

    public static void main(String[] args) throws IOException, DocumentException {

        ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
        templateResolver.setSuffix(".html");
        templateResolver.setPrefix("/WEB-INF/template/");
        templateResolver.setTemplateMode(HTML);
        templateResolver.setCharacterEncoding(StandardCharsets.UTF_8.toString());

        TemplateEngine templateEngine = new TemplateEngine();
        templateEngine.setTemplateResolver(templateResolver);

        Context context = ContextSetter.createNewContext();

        String processedHtml = templateEngine.process("protocol", context);
        //        String processedHtml = LetterChanger.changeLetters(protocol);

        createPdfFromProcessedHtml(processedHtml, "protocol.pdf");
        Desktop.getDesktop()
               .open(new File("protocol.pdf"));

    }

    public static void createPdfFromProcessedHtml(String processedHtml, String fileName) throws IOException, DocumentException {
        Document document = new Document();
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(fileName));

        document.open();

        XMLWorkerHelper worker = XMLWorkerHelper.getInstance();
        InputStream is = new ByteArrayInputStream(processedHtml.getBytes(StandardCharsets.UTF_8));
        worker.parseXHtml(writer, document, is, Charset.forName("UTF-8"));

        document.close();
    }
}
